

// Where the distances between each city are organized in a matrix. An array of distances is also built.
public class Map {

	public static double[][] init (Location[] locationArray, int NUMBER_OF_CITIES)
	{

		double[][] map = new double [NUMBER_OF_CITIES][NUMBER_OF_CITIES];
		int i = 0;
		int j = 0;

		for( i = 0 ; i < NUMBER_OF_CITIES ; i++ )  
			for ( j = 0 ; j < NUMBER_OF_CITIES ; j++ )
				if (i == j)
					map[i][j] = 0.0;
				else
					map [i][j] = XtraMethods.calculate_distance (locationArray[i], locationArray[j]);

		return map;

	}

	public static double[] get_distance (double[][] map, int NUMBER_OF_CITIES)
	{
		int i = 0;
		int j = 0;
		int k = 0;

		double[] distances = new double[(NUMBER_OF_CITIES*(NUMBER_OF_CITIES - 1)) /2];


		for (j = 0; j < NUMBER_OF_CITIES; j++)
			for (i = 1; i < NUMBER_OF_CITIES; i++)
				if (j < i)
					distances[k++] = map [i][j];

		return distances;


	}

}
