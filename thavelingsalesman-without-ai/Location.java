

// Where all the information about each location is collected and stored. 
// An array of locations is also built.
public class Location {

	//declare variables
	private double latitude;
	private double longitude;
	private String name;

	//default constructor
	public Location ()
	{
		this (0.0 , 0.0 ,"?");
	}

	//constructor with parameters
	public Location (double latitude, double longitude, String name)
	{
		this.latitude = latitude;
		this.longitude = longitude;
		this.name = name;
	}

	//set methods
	public void set_latitude (double latitude)
	{
		this.latitude = latitude;
	}

	public void set_longitude (double longitude)
	{
		this.longitude = longitude;
	}

	public void set_name (String name)
	{
		this.name = name;
	}

	//get methods
	public double get_latitude ()
	{
		return latitude;
	}

	public double get_longitude ()
	{
		return longitude;
	}

	public String get_name ()
	{
		return name;
	}

	//ask method
	public void ask_location() throws NumberFormatException
	{
		do {	
			latitude = 999;
			try {
				latitude = Console.askDouble(" Insert latitude: ");
				set_latitude (latitude);
			}
			catch(NumberFormatException e)
			{
				System.out.println(" Input must be an integer!!");
			}
		} while (latitude == 999);

		do {
			longitude = 999;
			try {
				longitude = Console.askDouble(" Insert longitude: ");
				set_longitude(longitude);
			}
			catch(NumberFormatException e)
			{
				System.out.println(" Input must be an integer!!");
			}
		} while (longitude == 999);

		name = Console.askString(" Insert name for location: ");
		set_name (name);

		System.out.println();
	}
	
	//print method
	public void print_location()
	{
		System.out.println("-----------------------------");
		System.out.println("Latitude: " +latitude);
		System.out.println("Longitude: " +longitude);
		System.out.println("Name: " +name);
		System.out.println("-----------------------------\n");
	}

	//toString method
	public String toString()
	{
		return "\n-----------------------------\nLatitude:" +latitude  + "\nLongitude: " +longitude + "\nName: " +name  + "\n-----------------------------\n";
	} 

	public static Location[] launchArray(int NUMBER_OF_CITIES) 
	{

		Location[] locationArray = new Location[NUMBER_OF_CITIES]; 
		int i = 0;

		for (i = 0; i < NUMBER_OF_CITIES; i++)
		{
			locationArray[i] = new Location();
			locationArray[i].ask_location();
		}

		return locationArray;

	}
}
