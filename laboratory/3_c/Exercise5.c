#include <stdio.h>

int main()
{
    int length = 0;
    int i;
    int j;
    int temp;
 
    printf(" Enter the length of the array: ");
    scanf("%d", &length);

    int array[length];
    int tempArray[length];

    printf(" Enter the value os the position of the array: \n");

    for (i = 0; i < length; i++)
      {	
	printf(" %d: ", i);
	scanf("%d", &temp);
	array[i] = temp;
      }

    int greatest = array[0];
    int lower = array[0];
    int index = 0;

    for(i = 0; i < length; i++)
      if(array[i] > greatest)
	greatest = array[i];

    for (j = 0; j < length; j++)
      {
	for (i = 0; i < length; i++)
	  if (array[i] <= lower)
	    {
	      lower = array[i];
	      index = i;
	    }

	tempArray[j] = array[index];
	array[index] = greatest;
	lower = array[0];
      }

    for (i = 0; i < length; i++)
      array[i] = tempArray[i];
		
    for (i = 0; i < length; i++)
      printf(" %d ", array[i]);

    printf(" \n");
}
