


public class Exercise1 {

	/**
	 * 1. Check if a number is even or odd
	 */
	public static void main(String[] args) {
		
			int number = Console.askInt(" Give a number to be checked: ");
		
			if (number % 2 == 0)
				System.out.println("\n This number is even!!");
			else
				System.out.println("\n This number is odd!!");

	}

}
