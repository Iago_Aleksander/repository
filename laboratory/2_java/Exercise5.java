


public class Exercise5 {

	/**
	 * 5. Sort an integer array without using API methods
	 */
	public static void main(String[] args) {
		
		int length = Console.askInt(" Enter the length of the array: ");
		int[] array = new int[length];
		int[] tempArray = new int[length];
		
		System.out.println(" Enter the value of the position of the array: ");
		for (int i = 0; i < length; i++)
			array[i] = Console.askInt(" " + i + " : ");
		
		int greatest = array[0];
		int lower = array[0];
		int index = 0;
		
		for (int i = 0; i < length; i++)
			if (array[i] > greatest)
				greatest = array[i];
		
		for (int j = 0; j < length; j++)
		{
			for (int i = 0; i < length; i++)
				if (array[i] <= lower)
				{
					lower = array[i];
					index = i;
				}
		
			tempArray[j] = array[index];
			array[index] = greatest;
			lower = array[0];
		}
		
		for (int i = 0; i < length; i++)
		array[i] = tempArray[i];
		
		for (int i = 0; i < length; i++)
			System.out.println(array[i]);

	}

}
