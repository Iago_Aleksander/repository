#include <stdio.h>

int fatorial (int n)
{
  if(n == 0)
    return 0;
  if(n == 1)
    return 1;
  else 
    return (n * fatorial(n-1));
}

int main()
{
    int number;
    unsigned int fatorial2;

    printf(" Enter the number to be considered: ");
    scanf("%d", &number);

    fatorial2 = fatorial(number);
    printf(" The fatorial of %d is %d !!! ", number,fatorial2);
    
    printf("\n");
}
