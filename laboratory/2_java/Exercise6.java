


public class Exercise6 {

	// 6. Reverse any String without using StringBuffer
	
	public static void main(String[] args) {
		
		String string = Console.askString(" Insert the sentence to be inverted: ");
		
		char[] stringArray = string.toCharArray();
		char[] temp = new char[stringArray.length];
	    
	    for (int i = 0; i < stringArray.length; i++)
	    	temp[stringArray.length - 1 - i] = stringArray[i];
	    
	    string = new String(temp);
	    
	    System.out.println("\n The inverted sentence is:\n " + string);
	    
	}

}
