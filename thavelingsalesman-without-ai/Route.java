

// Where routes are calculated and the best result is found. It also prints these information.
public class Route {

	private static int cont;
	private static double[] resultsArray;
	private static Location[] tempArray;
	private static Location[][] routes;
	private static int NUMBER_OF_CITIES;
	private static boolean show_routes;

	public static void permuta(int N_OF_CITIES, Location[] locationArray, boolean routesON) 
	{
		NUMBER_OF_CITIES = N_OF_CITIES;
		resultsArray = new double[XtraMethods.fatorial(NUMBER_OF_CITIES)];
		tempArray = new Location[NUMBER_OF_CITIES];
		routes = new Location[XtraMethods.fatorial(NUMBER_OF_CITIES)][NUMBER_OF_CITIES];
		show_routes = routesON;
		cont = 0;

		permuta(locationArray, 0);
	}

	private static void permuta(Location[] locationArray, int n) 
	{
		if (n == NUMBER_OF_CITIES) 
		{

			imprime(locationArray);
			cont++;
		} 
		else 
		{
			for (int i = 0; i < NUMBER_OF_CITIES; i++) 
			{
				boolean achou = false;
				for (int j = 0; j < n; j++) 
				{
					//if (routes[cont][j] == locationArray[i])
					if (tempArray[j] == locationArray[i])
						achou = true;
				}

				if (!achou) 
				{
					//routes[cont][n] = locationArray[i];
					tempArray[n] = locationArray[i];
					permuta(locationArray, n + 1);
				}
			}
		}
	}

	private static void imprime(Location[] locationArray) 
	{
		if (show_routes == true)
		{
			System.out.println();
			System.out.print("(" + cont + ") : ");
		}


		for (int i = 0; i < NUMBER_OF_CITIES; i++)
		{
			routes[cont][i] = tempArray[i];
			if (show_routes == true)
				System.out.print(routes[cont][i].get_name() + " -> ");
		}

		calculate(routes);

	}

	private static void calculate (Location[][] routes)
	{
		double result = 0;

		for (int j = 0; j < NUMBER_OF_CITIES - 1; j++)
			result += XtraMethods.calculate_distance(routes[cont][j], routes[cont][j+1]);

		if (show_routes == true)
			System.out.print(result);

		resultsArray[cont] = result;

	}

	public static void get_bestResult ()
	{
		double bestResult = resultsArray[0];
		int k = 0;

		for (int i = 0; i < cont; i++)
			if (resultsArray[i] < bestResult)
			{
				bestResult = resultsArray[i];
				k = i;
			}

		System.out.println("\nThe best Rote is: " );
		for (int j = 0; j < NUMBER_OF_CITIES; j++)
			System.out.print(routes[k][j].get_name() + " -> ");

		System.out.print(bestResult);
		System.out.println();

	}
}



