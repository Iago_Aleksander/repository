#include <stdio.h>

int main()
{
    int number;
    int power_of_2 = 0;
    int i = 1;
    int rest = 0;
    int temp;

    printf(" Give a number to be checked: ");
    scanf("%d", &number);

    temp = number;

    for (i = 0; i <= number; i++)
      {
	if (number == 1)
	  power_of_2 = 1;
	if (rest != 0)
	  number = 0;
	else if(temp == 2)
	  power_of_2 = 1;
	
	rest = temp % 2;
        temp = temp/2;
      }

    if (power_of_2  == 1)
        printf("The number is power of two!!\n");
    else
        printf(" The number is not power of two!!\n");
}
