


public class Exercise4 {

	/**
	 * 4. Find out if a number is power of 2
	 */
	public static void main(String[] args) {
		
		int number = Console.askInt(" Give a number to be checked: ");
		int temp = number;
		boolean power_of_2 = false;
		int rest = 0;
		
		for (int i = 0; i < Math.sqrt(number); i++)
		{
			if (rest != 0)
				number = 0;
			else if (temp == 2)
				power_of_2 = true;	
			
			rest = temp % 2;
			temp = temp/2;
		}
		
		if (power_of_2)
			System.out.println("\n This number is a power of two!!");
		else
			System.out.println("\n This number is not a power of two!!");
			

	}

}
