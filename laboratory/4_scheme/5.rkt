;; 5. Sort an integer array without using API methods

#lang racket

(define (insert x lst)
  (if (null? lst)
      (list x)
      (if (<= x (car lst))
          (cons x lst)
          (cons (car lst)(insert x (cdr lst))))))
 
(define (sort lst)
  (if (null? lst)
      '()
      (insert (car lst)(sort (cdr lst)))))
 
(sort '(21 17 5 13 7 2 0 4))