


public class Exercise3 {

	/**
	 3. Check if a number is palindrome
	 */
	public static void main(String[] args) {
		
		long n = Console.askInt(" Give a number to be checked: ");
		int length = (int)(Math.log10(n)+1);
		boolean palindrome = true;
		int j = length-1;
		
		int[] numberVet = new int[length];
		
		for (int i = 0; i < length; i++)
			{
				numberVet[i] = (int)(n % 10);
				n = n/10;
			}
		
		for (int i = 0; i < length/2; i++)
		{
				if (numberVet[i] != numberVet[j])
					palindrome = false;
				j--;
		}
		
		
		if (palindrome)
			System.out.println("\n This number is a palindrome!!");
		else
			System.out.println("\n This number is not a palindrome!!");
		


	}

}
