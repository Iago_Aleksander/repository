#include <stdio.h>

int main()
{
    int number;
    printf(" Give a number to be checked: ");
    scanf("%d", &number);

    if (number % 2 == 0)
        printf("\n This number is even!!\n");
    else
        printf("\n This number is odd!!\n");
}
