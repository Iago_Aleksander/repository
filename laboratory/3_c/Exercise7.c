#include <stdio.h>

int main()
{
    int number;
    int fibonacciNumber1 = 0;
    int fibonacciNumber2 = 1;

    int temp;

    printf(" Until which number the Fibonacci sequence should be shown?: ");
    scanf("%d", &number);

    while (fibonacciNumber1 <= number)
      {
        printf(" %d ", fibonacciNumber1);
	temp = fibonacciNumber1;
	fibonacciNumber1 = fibonacciNumber2;
	fibonacciNumber2 = fibonacciNumber1 + temp;
      }
   
      printf("\n");
}
