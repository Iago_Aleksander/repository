

// Finds the distance between each city and calculates a factorial of a number.
public class XtraMethods {

	public static double calculate_distance(Location a, Location b)
	{
		double distance = Math.sqrt ((b.get_latitude() - a.get_latitude())*(b.get_latitude() - a.get_latitude()) + (b.get_longitude() - a.get_longitude())*(b.get_longitude() - a.get_longitude()));
		return distance;
	}

	public static int fatorial (int n)
	{
		if(n == 0)
			return 1;
		else 
			return n * fatorial(n-1);
	}
}
